mavenJob("apiaccountsci_copy") {
    scm {
       	git {
          remote {
            branch('*/master')
              url('https://bitbucket.org/jorge-pais/bbva-apis-api-accounts.git')
              credentials('jpais-bitbucket')
          }
        }
    }
    triggers {
        snapshotDependencies(true)
        gitlabPush()
    }
    wrappers{
        credentialsBinding{
            string('dockereg','dockerRepository')
            string('releases','mavenReleases')
            string('snapshots','mavenSnapshots')
            string('nexus','nexusURL')
        }
    }
    goals('clean package deploy -DdockerRepository=${dockereg} -DmavenReleases=${releases} -DmavenSnapshots=${snapshots} -DnexusURL=${nexus}')
}
