job("dockerfile_pipeline") {
    scm {
       	git {
          remote {
            branch('*/master')
              url('https://bitbucket.org/jorge-pais/bbva-apis-api-accounts.git')
              credentials('jpais-bitbucket')
          }
        }
    }
    triggers {
        gitlabPush()
    }
    wrappers{
        credentialsBinding{
            string('dockereg','dockerRepository')
            string('releases','mavenReleases')
            string('snapshots','mavenSnapshots')
            string('nexus','nexusURL')
            string('dockerregpass','docker-reg-pass')

        }
    }
    steps {
        shell('echo start')
        maven('clean package -DdockerRepository=${dockereg} -DmavenReleases=${releases} -DmavenSnapshots=${snapshots} -DnexusURL=${nexus}')
        shell('wget  && ./dockerbuildanddeploy.sh')
        shell('echo end')
    }
}
