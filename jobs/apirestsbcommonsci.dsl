mavenJob("apirestsbcommonsci") {
    scm {
       	git {
       		remote {
		        branch('*/master')
       			url('https://bitbucket.org/jorge-pais/bbva-apis-rest-sb-commons.git')
				credentials('jpais-bitbucket')
       		}
        }
    }
    triggers {
        snapshotDependencies(true)
        gitlabPush()
    }
    wrappers{
        credentialsBinding{
            string('docker','dockerRepository')
            string('releases','mavenReleases')
            string('snapshots','mavenSnapshots')
            string('nexus','nexusURL')
        }
    }
    goals('clean deploy -DmavenReleases=${releases} -DmavenSnapshots=${snapshots} -DnexusURL=${nexus}')
}