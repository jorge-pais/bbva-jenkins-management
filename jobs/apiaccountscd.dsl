job("apiaccountscd") {
    parameters {
        stringParam('VERSION', '0.0.1-SNAPSHOT', 'version a desplegar')
        stringParam('SERVICESNAME', 'api-accounts', 'nombre del servicio')
        stringParam('ARTIFACT', 'api-accounts', 'version a desplegar')
        stringParam('PORTPUBLIC', '7011', 'puerto publicado')
    }
    publishers {
        publishOverSsh {
            server('node-1') {
              credentials('root') {
                  pathToKey('/var/lib/jenkins/.ssh/jenkins-swarm')
              }
              transferSet {
                  execInPty(true)
                  execCommand('sudo docker login -u admin -p admin123 nexus.bbva.internal:8082')
              }
            }
        }
        publishOverSsh {
            server('node-1') {
              credentials('root') {
                  pathToKey('/var/lib/jenkins/.ssh/jenkins-swarm')
              }
              transferSet {
                  execInPty(true)
                  execCommand('sudo docker pull nexus.bbva.internal:8082/${ARTIFACT}:${VERSION}')
              }
            }
        }
        publishOverSsh {
            server('node-1') {
              credentials('root') {
                  pathToKey('/var/lib/jenkins/.ssh/jenkins-swarm')
              }
              transferSet {
                  execInPty(true)
                  execCommand('sudo docker service rm ${SERVICESNAME}')
              }
            }
        }
        publishOverSsh {
            server('node-1') {
              credentials('root') {
                  pathToKey('/var/lib/jenkins/.ssh/jenkins-swarm')
              }
              transferSet {
                  execInPty(true)
                  execCommand('sudo docker service create -p ${PORTPUBLIC}:8080 --name ${SERVICESNAME} --with-registry-auth nexus.bbva.internal:8082/${ARTIFACT}:${VERSION}')
              }
            }
        }
    }
}