folder('CI'){
    description 'Jobs de build'
}

folder('CD-TST'){
    description 'Jobs de despliegue a ambiente de testing.'
}

folder('CD-INT'){
    description 'Jobs de despliegue a ambiente de integración.'
}

folder('CD-PRD'){
    description 'Jobs de despliegue a ambiente de producción.'
}

folder('Management'){
    description 'Jobs de administración y genéricos.'
}