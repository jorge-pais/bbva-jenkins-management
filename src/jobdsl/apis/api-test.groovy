new BBVAJenkinsCIwDockerfile(
    apiName: 'api-accounts',
    gitAPI: 'https://bitbucket.org/jorge-pais/bbva-apis-api-accounts.git',
    gitDockerfile: 'https://bitbucket.org/CamilaSerenaPyxis/dockerfile.git',
).build(this)

new BBVAJenkinsCD(
    environment: 'integracion',
    apiName: 'api-test'
).build(this)

new BBVAJenkinsCD(
    environment: 'testing',
    apiName: 'api-test'
).build(this)

new BBVAJenkinsCD(
    environment: 'produccion',
    apiName: 'api-test'
).build(this)