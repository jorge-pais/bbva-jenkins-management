new BBVAJenkinsCIwDockerfile(
    apiName: 'api-test2',
    gitURL: 'http://192.168.240.111/cicd/api-test.git',
).build(this)

new BBVAJenkinsCD(
    environment: 'integracion',
    apiName: 'api-test2'
).build(this)

new BBVAJenkinsCD(
    environment: 'testing',
    apiName: 'api-test2'
).build(this)

new BBVAJenkinsCD(
    environment: 'produccion',
    apiName: 'api-test2'
).build(this)
