import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.jobs.MavenJob

class BBVAJenkinsCI {

    String apiName
    String gitURL

    MavenJob build(DslFactory dslFactory) {

        dslFactory.mavenJob('CI/' + apiName + '-CI') {

            parameters {
		        stringParam("Branch", "", "Rama para la cual se quiere realizar el CI")
	        }

            environmentVariables {
		        groovy("""import hudson.model.*
                    def env = Thread.currentThread()?.executable.parent.builds[0].properties.get('envVars')
                    def map = [:]

                    if (env['gitlabSourceBranch'] != null) { 
                        map['sourceBranch'] = env['gitlabSourceBranch'] 
                    }else{
                        map['sourceBranch'] = env['Branch']
                    }
                    return map""")
		        loadFilesFromMaster(false)
		        keepSystemVariables(true)
		        keepBuildVariables(true)
		        overrideBuildParameters(false)
	        }
            
            logRotator(20,10,-1,-1)

            scm {
                git {
                    remote {
                        branches("*/\${sourceBranch}")
                        url(gitURL)
                        credentials('cicd Jenkins')
                    }
                }
            }
            
            triggers {
                snapshotDependencies(true)
                gitlabPush()
            }

            goals('clean package deploy -DimageVersion=0.${env.BUILD_NUMBER} -DnexusMavenReleases=${env.NEXUS_MAVEN_RELEASES} -DnexusMavenSnapshots=${env.NEXUS_MAVEN_SNAPSHOTS} -DnexusDocker=${env.NEXUS_DOCKER}')
        }
    }
}
