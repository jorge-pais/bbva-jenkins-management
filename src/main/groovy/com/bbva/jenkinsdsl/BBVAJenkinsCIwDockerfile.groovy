import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.jobs.FreeStyleJob

class BBVAJenkinsCIwDockerfile {

    String apiName
    String gitURL
    String gitAPI
    String gitDockerfile

    FreeStyleJob build(DslFactory dslFactory) {

        dslFactory.job('CI/' + apiName + '-CI-Dockerfile') {

            parameters {
                stringParam("Branch", "", "Rama para la cual se quiere realizar el CI")
            }

            environmentVariables {
                env('APINAME', apiName)
                groovy("""import hudson.model.*
                    def env = Thread.currentThread()?.executable.parent.builds[0].properties.get('envVars')
                    def map = [:]

                    if (env['gitlabSourceBranch'] != null) { 
                        map['sourceBranch'] = env['gitlabSourceBranch'] 
                    }else{
                        map['sourceBranch'] = env['Branch']
                    }
                    return map""")
                loadFilesFromMaster(false)
                keepSystemVariables(true)
                keepBuildVariables(true)
                overrideBuildParameters(false)
            }
            
            
            logRotator(20,10,-1,-1)

            scm {
                git {
                    remote {
                        branches("*/master")
                        url(gitAPI)
                        credentials('jpais-bitbucket')
                    }
                }
            }
            
            triggers {
                gitlabPush()
            }

            wrappers {
                credentialsBinding {
                    usernamePassword('NEXUSUSER','NEXUSPASS', 'nexusadmin')
                }
            }

            steps {
                maven('clean package -DnexusMavenReleases=${env.NEXUS_MAVEN_RELEASES} -DnexusMavenSnapshots=${env.NEXUS_MAVEN_SNAPSHOTS} -DbuildDirectory=$WORKSPACE/docker-workdir')
                shell('echo APIVERSION=$(mvn -o org.apache.maven.plugins:maven-help-plugin:2.1.1:evaluate -Dexpression=project.version | grep -v "\\[") > env.apiversion ')
                environmentVariables {propertiesFile('$WORKSPACE/env.apiversion')}
                shell('cd docker-workdir && wget https://bitbucket.org/CamilaSerenaPyxis/dockerfile/raw/88358f68e3ffb7b768c5e392c834020d90486709/Dockerfile && docker build --rm -t ${NEXUSURL}/${APINAME} . ')
                shell('docker tag ${NEXUSURL}/${APINAME} ${NEXUSURL}/${APINAME}:${APIVERSION}')
                shell('docker login -u ${NEXUSUSER} -p ${NEXUSPASS} ${NEXUSURL} && docker push ${NEXUSURL}/${APINAME}:${APIVERSION} && docker push ${NEXUSURL}/${APINAME}:latest')
                shell('docker rmi ${NEXUSURL}/${APINAME}:${APIVERSION} && docker rmi ${NEXUSURL}/${APINAME}:latest')
            }
        }
    }
}

