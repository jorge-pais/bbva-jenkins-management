import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.jobs.FreeStyleJob

class BBVAJenkinsCD {
    String environment
    String apiName

    private static String translateEnv(String env){
        if (env == 'integracion') return 'INT'
        else if (env == 'testing') return 'TST'
        else if (env == 'produccion') return 'PROD'
        else return ''
    }

    FreeStyleJob build(DslFactory dslFactory) {

        dslFactory.job('CD-'+ translateEnv(environment) + '/' + apiName + '-' + environment + '-CD') {
            parameters {
                stringParam('VERSION', '', 'Version de imagen a desplegar.')
                stringParam('PORT', '', 'Puerto de la API a desplegar.')
                stringParam('REPLICAS','1','Cantidad de replicas a desplegar.')
            }

            logRotator(30,10,-1,-1)

            steps{
                downstreamParameterized {
                    trigger('Management/MasterCD') {
                        block {
                            buildStepFailure('FAILURE')
                            failure('FAILURE')
                            unstable('UNSTABLE')
                        }
                        parameters {
                            predefinedProp("API_VERSION", '${VERSION}')
                            predefinedProp("API_PORT", '${PORT}')
                            predefinedProp("API_REPLICAS", '${REPLICAS}')
                            predefinedProp("API_ENVIRONMENT", environment)
                            predefinedProp("API_NAME", apiName)
                        }
	                }
	            }
	        }
        }
    }
}



