properties([
    parameters([
        string(name: 'API_VERSION', defaultValue: 'latest', description:'Version a desplegar.'),
        string(name: 'API_NAME', defaultValue: '', description:'Nombre de la API a desplegar tal cual está en el repositorio'),
        string(name: 'API_PORT', defaultValue: '', description:'Puerto de la API a desplegar.'),
        string(name: 'API_REPLICAS', defaultValue: '1', description:'Cantidad de replicas a desplegar.'),
        choice(name: 'API_ENVIRONMENT', defaultValue: 'testing', description:'Ambiente donde desplegar', choices: 'testing\nintegracion\nproduccion')
    ])
])

node{

    buildDiscarder(logRotator(numToKeepStr: '30', artifactNumToKeepStr: '10'))
        

    stage('Despliegue'){

        git(
            url: 'http://192.168.240.111/configuraciones/dockercompose.git',
            branch: 'dev',
            credentialsId: 'cicd Jenkins'
        )           

        def sshServer = ''

        def dockerStackCMD = "export API_VERSION=${params.API_VERSION}; export API_NAME=${params.API_NAME}; export API_PORT=${params.API_PORT}; export API_REPLICAS=${params.API_REPLICAS}; export API_ENVIRONMENT=${params.API_ENVIRONMENT}; docker stack deploy --with-registry-auth --compose-file=api-template.yml ${params.API_NAME}"

        
        if (params.API_ENVIRONMENT == 'testing'){
            sshServer = 'node-apis-testing'
        }else if(params.API_ENVIRONMENT == 'integracion'){
            sshServer = 'node-apis-integracion'
        }else if(params.API_ENVIRONMENT == 'produccion'){
            sshServer = 'node-apis-produccion'
        }
        
        def composeSourceFile = "api-template.yml"

        sshPublisher(
            publishers: [
                sshPublisherDesc(
                    configName: sshServer, 
                    transfers: [
                        sshTransfer(     
                            sourceFiles: composeSourceFile,                       
                            execCommand: dockerStackCMD, 
                            execTimeout: 120000, 
                            usePty: true
                        )
                    ], 
                    usePromotionTimestamp: false, 
                    useWorkspaceInPromotion: false, 
                    verbose: false                        
                )                   
            ]
        )
    }  
}




